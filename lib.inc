%define	NULL_CHAR 		0
%define TAB_CHAR 		0X9
%define	SPACE_CHAR 		0x20
%define	ENDL_CHAR 		0xA	
%define	EXIT 			60
%define	STDOUT 		    1	
%define	MIN 		    "0"	
%define	MAX 		    "9"		
%define MAX_DEC_DIGITS_64BIT	20
	SECTION .text
exit:
	mov	rax, EXIT		
	syscall			


; compute length of null-terminated string
; input
;	rdi : string addr
; output:
;	rax : string size
string_length:
	xor	rax , rax
.loop:
	cmp	byte[rdi + rax] , NULL_CHAR
	je	.end
	inc	rax	
	jmp	.loop	
.end:
	ret 

; input rdi : pointer to null-term string
print_string:
	call	string_length	; rax now contains string length
	mov	rsi , rdi
	mov	rdx , rax
	mov	rdi , STDOUT
	mov	rax , STDOUT
	syscall
	ret

; input rdi : char-code
print_char:
	push 	rdi  
	mov	rsi , rsp
	add	rsp , 8
	mov	rdx , STDOUT
	mov	rax , STDOUT
	mov	rdi , STDOUT
	syscall
	ret

print_newline:
	mov	rdi , ENDL_CHAR
	call	print_char
	ret

; input rdi : 64bit unsigned int
print_uint:
	mov	rax , rdi
	xor	rsi , rsi
	mov	r10 , 10
	mov	r9 , rsp
	dec	rsp
	mov	byte [rsp] , NULL_CHAR		; null-terminator 
.div10:	
	xor	rdx , rdx
	div	r10
	add	rdx , '0'
	dec	rsp
	mov	byte [rsp] , dl		; 1 byte added to stack
	inc	rsi
	cmp	rax , 0
	jne	.div10
	mov	rdi , rsp	; pointer to number formed in stack
	call	print_string
	mov	rsp , r9	; restore rsp
	ret	

;inpt rdi
;output syscall print
print_int:
	mov	rax , rdi
	cmp	rax , 0
	jl	.negative
.positive:
	call 	print_uint
	ret
.negative:
	neg	rax
	mov	r10 , rax
	mov	rdi , '-'
	call	print_char
	mov	rdi , r10
	call	print_uint
	ret

;rdi pointer to first string , rsi pointer to second string
;rax returns 1 or 0
string_equals:
.loop:
	mov	dl , byte [rdi]
	cmp	dl , byte [rsi]
	jne	.false
	cmp	byte[rdi] , NULL_CHAR
	je     	.true
	inc	rdi
	inc	rsi
	jmp	.loop
.true:
	mov	rax , 1
	ret
.false:
	xor	rax , rax
	ret

;output : al - char 
read_char:
	xor	rax , rax
	dec	rsp
	mov	byte[rsp] , NULL_CHAR
	xor	rdi , rdi
	mov	rsi , rsp
	mov	rdx , 1
	syscall
	mov	al , byte[rsp]
	inc	rsp
	ret

;input: rdi : pointer to buf
;	rsi : size of buf
read_word:
	xor	rax , rax
	xor	rdx , rdx	;counter
.loop:
	cmp	rsi , rdx	; we need rsi  > rdx , due to NULL
	jle	.fail
	push	rsi
	push	rdi	
	push	rdx
	call	read_char	;al now has char from stdin
	pop	rdx
	pop	rdi	
	pop	rsi		
	cmp	al , TAB_CHAR
	je	.check
	cmp	al , SPACE_CHAR
	je	.check
	cmp	al , ENDL_CHAR
	je	.check
	mov	byte [rdi + rdx] , al
	cmp	al , NULL_CHAR
	je	.success
	inc	rdx
	jmp	.loop
.check:	
	test	rdx , rdx
	jz	.loop
	mov	byte [rdi + rdx] , NULL_CHAR	;if word ends with space char, adding null-terminator	
.success:
	mov	rax , rdi
	ret	
.fail:
	xor	rax , rax
	ret

parse_uint:
        xor     rax , rax
        xor     rdx , rdx
        mov     r10 , 10
        xor     rcx , rcx
.loop:
        mov     cl , byte [rdi + rdx]
        test    cl , cl
        jz      .check
        cmp     cl , MIN
        jl      .check
        cmp     cl , MAX
        jg      .check
        inc     rdx
        sub     cl , MIN
        push	rdx
	mul     r10
	pop	rdx
        add     rax , rcx
        cmp     rdx , MAX_DEC_DIGITS_64BIT
        jg      .fail
        jmp     .loop
.check:
        test	rdx , rdx
	jz	.fail
	ret
.fail:
        xor     rdx ,rdx
        ret

;input 	rdi : pointer to str
;output	rax : signed int , rdx : length of int 
parse_int:
	push	rdi
	call	read_char	; al now has first char of str 
	pop	rdi
	cmp	al , '-'
	je	.signed
	cmp	al , '+' 		
	je	.signed
	call	parse_uint
	ret
.signed:
	inc	rdi
	push	rax		;save rax for sign
	call	parse_uint
	pop	rcx		;restore sign to rcx
	test	rdx , rdx
	jz	.fail
	inc	rdx
	cmp	cl , '-'
	je	.neg
	ret
.neg:
	neg	rax
	ret
.fail:
	xor	rdx , rdx
	ret

;input rdi : pointer to str , rsi : pointer to buffer , rdx : length of buffer
;output rax : size of str , if failed 0
string_copy:
	call	string_length
	xor	rcx , rcx
	cmp	rdx , rax
	jle	.fail
.loop:	
	mov	dl , byte[rdi + rcx]
	mov	byte [rsi + rcx ] , dl
	test	dl , dl
	jz	.success
	inc	rcx
	jmp	.loop
.fail:
	xor	rax , rax
	ret
.success:
	mov	rax , rcx
	ret

